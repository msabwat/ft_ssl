import hashlib
import sys
import os

args = [os.fsencode(arg) for arg in sys.argv]

#print(hashlib.md5(args[1]).hexdigest())
print(hashlib.sha256(args[1]).hexdigest())


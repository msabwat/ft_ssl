f = open("test-b64", "wb")
for i in range(pow(2, 8)):
    f.write(i.to_bytes(3, byteorder='little'))
    print(0,0,i)
for i in range(pow(2, 8)):
    f.write(((i << 8) | 0b11111111).to_bytes(3, byteorder='little'))
    print(i, 0b11111111)
for i in range(pow(2, 8)):
    f.write(((i << 16) | (0b11111111) | 0b11111111).to_bytes(3, byteorder='little'))
    print(i, ( 0b11111111), 0b11111111)

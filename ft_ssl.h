#ifndef FT_SSL_H
#define FT_SSL_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct s_opts {
    const char *opt;
    const char *desc;
} t_opts;

typedef struct s_md_cbs {
    void (*md_hash) (char *block, uint64_t block_size, char** result, size_t total_size);
    char *(*init_hash) (void);
    void (*print_hash) (char *result, size_t size);
    const char *hash_name;
} t_md_cbs;

typedef enum e_cipher_mode {
    NONE=0,
    DES_MODE,
    DES_CBC_MODE,
    DES_ECB_MODE,
} t_mode;

typedef struct s_cmd {
    const char *name;
    void (*pf_runcmd)(int ac, char **av, t_mode variant);
    t_mode variant;
} t_cmd;

typedef struct s_cipher_opts {
    bool encode;
    bool b64;
    bool cbc;
    uint64_t key; // derived from pass + salt + iv (if mode is cbc)
    uint64_t iv;
    uint64_t salt; 
    char *in_password;
    int in_fd;
    int out_fd;
    unsigned char **subkeys;
} t_cipher_opts;

typedef struct s_cipher_cbs {
    unsigned char *out_buf;
    size_t (*decode)(const unsigned char *input, size_t len, unsigned char **out_buf, t_cipher_opts opts);
    void (*encode)(const unsigned char *input, size_t len, unsigned char **out_buf, t_cipher_opts opts);
    const char *cipher_name;
    t_mode variant;
} t_cipher_cbs;

int validate_args(int ac, char **av);

void run_md5 (int ac, char **av, t_mode variant);
void md5_round (unsigned char *buf, char **result);
void md5_block (char *chunk, uint64_t chunk_size, char **result, size_t total_size);
char *init_md5_hash(void);

void run_sha256 (int ac, char **av, t_mode variant);
void sha256_round (unsigned char *buf, char **result);
void sha256_block (char *chunk, uint64_t chunk_size, char **result, size_t total_size);
char *init_sha256_hash(void);

void handle_md_options(int ac, char **av, t_md_cbs cbs);
void handle_cipher_options(int ac, char **av, t_cipher_cbs cbs);

void run_base64 (int ac, char **av, t_mode variant);
void encode_base64(const unsigned char *input, size_t len, unsigned char **out_buf, t_cipher_opts opts);
size_t decode_base64(const unsigned char *input, size_t len, unsigned char **out_buf, t_cipher_opts opts);
void run_des (int ac, char **av, t_mode variant);

void create_subkeys(uint64_t key, unsigned char **subkeys);
void des_process_block(uint64_t in, unsigned char **subkeys, uint64_t *out, bool decode);
#ifdef DEBUG
void dbg_printBits(size_t size, void *ptr);
#endif
void memcpy_be(void *dst, const void *src, int nbytes);
int is_valid_b64(char c);
int is_valid_hex_opt(const char *in);
unsigned char *get_64random();
uint64_t get_random_8bytes();
// unsigned char *pbkdf1(uint32_t *(*hash_fun)(char *input, size_t size), char *password, char *salt, size_t iter);
// uint32_t *pbkdf_md5_hash(char *input, size_t size);
uint64_t pbkdf2_dKlen_8bytes(char *password, uint64_t salt, unsigned count);
uint64_t atohex(char *in);
#endif

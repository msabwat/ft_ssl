#include "ft_ssl.h"
#include "libft.h"

static unsigned char row_from_sextet(uint64_t sextet) {
    unsigned char b1 = (unsigned char) (sextet & 1);
    unsigned char b2 = (unsigned char) ((sextet & 32) >> 5);
    return (b2 << 1) | b1;
}

static unsigned char col_from_sextet(uint64_t sextet) {
    return (unsigned char) ((sextet >> 1) & 15);
}

static void generate_keyplus(uint64_t key, unsigned char *kplus) {
    unsigned char pc1_table[56] = {
        57, 49, 41, 33, 25, 17, 9,
        1, 58, 50, 42, 34, 26, 18,
        10, 2, 59, 51, 43, 35, 27,
        19, 11, 3, 60, 52, 44, 36,
        63, 55, 47, 39, 31, 23, 15,
        7, 62, 54, 46, 38, 30, 22,
        14, 6, 61, 53, 45, 37, 29,
        21, 13, 5, 28, 20, 12, 4
    };

    unsigned char i = 0;
    unsigned char tmp = 0;
    for(unsigned j = 0; j < 7; j++) {
        for (unsigned k = 0; k < 8; k++) {
            tmp = ((key >> (64 - pc1_table[i])) & 0b00000001);
            if (k == 0)
                kplus[j] = tmp;
            else
                kplus[j] = tmp | (kplus[j] << 1);            
            i++;
        }
    }
}

static unsigned int left_shift(unsigned int key_half, unsigned shift) {
    unsigned int half = key_half;

    half = half & 0x0FFFFFFF;
    half = half << shift;
    half = (half | (half >> 28));
    half = half & 0x0FFFFFFF;
    return half;
}

static void generate_subkey(unsigned char **subkey, uint64_t key, unsigned index) {
    unsigned char pc2_table[48] = {
        14, 17, 11, 24, 1, 5,
        3, 28, 15, 6, 21, 10,
        23, 19, 12, 4, 26, 8,
        16, 7, 27, 20, 13, 2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 45, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29, 32
    };

    unsigned char tmp = 0;
    unsigned int cur = 0;
    uint64_t res = 0;
    unsigned i = 0;

    for (char j = 0; j < 8; j++) {
        for (char k = 0; k < 6; k++) {
            tmp = ((key >> (56 - pc2_table[i])) & 0b00000001);
            if (k == 0)
                cur = tmp;
            else
                cur = tmp | (cur << 1);
            i++;
        }
        if (j != 0) {
            res = (res << 6);
            res = res | cur;
        }
        else
            res = cur;
    }
    for (int i = 0; i < 6; i++) {
        subkey[index][i] = (res >> (i * 8)) & 255;
    }
}

static void generate_subkeys(unsigned char *kplus, unsigned char **subkeys, int num) {
    unsigned int l = 0; // left split
    unsigned int r = 0; // right split

    for (int i = 0; i < 4; i++) {
        l = l | kplus[i];
        if (i < 3)
            l = (l << 8);
    }
    for (int j = 3; j < 7; j++) {
        r = r | kplus[j];
        if (j < 6)
            r = (r << 8);
    }
    l = l >> 4;
    r = r << 4;
    r = r >> 4;

    unsigned shift_table[16] = {
        1,1,2,2,
        2,2,2,2,
        1,2,2,2,
        2,2,2,1
    };
    uint64_t key = 0; // 3.5 + 3.5
    
    for (int i = 0; i < num; i++) {
        key = 0;
        l = left_shift(l, shift_table[i]);
        r = left_shift(r, shift_table[i]);
        key = key | l;
        key = (key << 28) | r;
        generate_subkey((unsigned char **)subkeys, key, i);
    }
}

/*
  fill subkeys variable with 16 - 48 bit variables from   
  the generated key (from the pbkdf)
 */
void create_subkeys(uint64_t key, unsigned char **subkeys) {
    unsigned char kplus[7];
    ft_memset(kplus, 0, 7);

	// K+
    generate_keyplus(key, kplus);

    // K1 - K16
    generate_subkeys(kplus, subkeys, 16);
}

static unsigned int des_fun( unsigned int in_p_r, unsigned char *subkey) {
    // expand in_p_r to 48 bits
    unsigned char exp [] = {
        32, 1, 2, 3, 4, 5,
        4, 5, 6, 7, 8, 9,
        8, 9, 10, 11, 12, 13,
        12, 13, 14, 15, 16, 17,
        16, 17, 18, 19, 20, 21,
        20, 21, 22, 23, 24, 25,
        24, 25, 26, 27, 28, 29,
        28, 29, 30, 31, 32, 1
    };
    
    uint64_t r = 0;
    unsigned char tmp = 0;
    for (unsigned char i = 0; i < 48; i++) {
        tmp = ((in_p_r >> (32 - exp[i])) & 0b00000001);
        if (i == 0)
            r = tmp;
        else
            r = tmp | (r << 1);
    }
    uint64_t result = 0;
    unsigned char i = 0;
    unsigned char tmp_subkey[6];
    for (i = 0; i < 6; i++) {
        tmp_subkey[i] = (subkey[i] ^ (r >> ((i * 8) & 255)));
    }
    i = 5;
    while (i) {
        result = result | tmp_subkey[i--];
        result = result << 8;
    }
    result = result | tmp_subkey[0];

    // generate 32 bits from 48 bits
    unsigned char s[8][4][16] = {
        {
            {
                14, 4, 13, 1, 2, 15, 11, 8, 3,
                10, 6, 12, 5, 9, 0, 7
            },
            {
                0, 15, 7, 4, 14, 2, 13, 1,
                10, 6, 12, 11, 9, 5, 3, 8
            },
            {
                4, 1, 14, 8, 13, 6, 2, 11,
                15, 12, 9, 7, 3, 10, 5, 0
            },
            {
                15, 12, 8, 2, 4, 9, 1, 7,
                5, 11, 3, 14, 10, 0, 6, 13
            }
        },
        {
            {
                15, 1, 8, 14, 6, 11, 3,
                4, 9, 7, 2, 13, 12, 0, 5, 10
            },
            {
                3, 13, 4, 7, 15, 2, 8, 14,
                12, 0, 1, 10, 6, 9, 11, 5
            },
            {
                0, 14, 7, 11, 10, 4, 13, 1,
                5, 8, 12, 6, 9, 3, 2, 15
            },
            {
                13, 8, 10, 1, 3, 15, 4, 2,
                11, 6, 7, 12, 0, 5, 14, 9
            }
        },
        {
            {
                10, 0, 9, 14, 6, 3, 15,
                5, 1, 13, 12, 7, 11, 4, 2, 8
            },
            {
                13, 7, 0, 9, 3, 4, 6, 10,
                2, 8, 5, 14, 12, 11, 15, 1
            },
            {
                13, 6, 4, 9, 8, 15, 3, 0, 11,
                1, 2, 12, 5, 10, 14, 7
            },
            {
                1, 10, 13, 0, 6, 9, 8, 7,
                4, 15, 14, 3, 11, 5, 2, 12
            }
        },
        {
            {
                7, 13, 14, 3, 0, 6, 9, 10, 1,
                2, 8, 5, 11, 12, 4, 15
            },
            {
                13, 8, 11, 5, 6, 15, 0, 3,
                4, 7, 2, 12, 1, 10, 14, 9
            },
            {
                10, 6, 9, 0, 12, 11, 7, 13, 15,
                1, 3, 14, 5, 2, 8, 4
            },
            {
                3, 15, 0, 6, 10, 1, 13, 8,
                9, 4, 5, 11, 12, 7, 2, 14
            }
        },
        {
            {
                2, 12, 4, 1, 7, 10, 11, 6,
                8, 5, 3, 15, 13, 0, 14, 9
            },
            {
                14, 11, 2, 12, 4, 7, 13, 1,
                5, 0, 15, 10, 3, 9, 8, 6
            },
            {
                4, 2, 1, 11, 10, 13, 7, 8,
                15, 9, 12, 5, 6, 3, 0, 14
            },
            {
                11, 8, 12, 7, 1, 14, 2, 13,
                6, 15, 0, 9, 10, 4, 5, 3
            }
        },
        {
            {
                12, 1, 10, 15, 9, 2, 6, 8, 0,
                13, 3, 4, 14, 7, 5, 11
            },
            {
                10, 15, 4, 2, 7, 12, 9, 5,
                6, 1, 13, 14, 0, 11, 3, 8
            },
            {
                9, 14, 15, 5, 2, 8, 12, 3,
                7, 0, 4, 10, 1, 13, 11, 6
            },
            {
                4, 3, 2, 12, 9, 5, 15, 10,
                11, 14, 1, 7, 6, 0, 8, 13
            }
        },
        {
            {
                4, 11, 2, 14, 15, 0, 8, 13,
                3, 12, 9, 7, 5, 10, 6, 1
            },
            {
                13, 0, 11, 7, 4, 9, 1, 10,
                14, 3, 5, 12, 2, 15, 8, 6
            },
            {
                1, 4, 11, 13, 12, 3, 7,
                14, 10, 15, 6, 8, 0, 5, 9, 2
            },
            {
                6, 11, 13, 8, 1, 4, 10, 7,
                9, 5, 0, 15, 14, 2, 3, 12
            }
        },
        {
            {
                13, 2, 8, 4, 6, 15, 11,
                1, 10, 9, 3, 14, 5, 0, 12, 7
            },
            {
                1, 15, 13, 8, 10, 3, 7, 4,
                12, 5, 6, 11, 0, 14, 9, 2
            },
            {
                7, 11, 4, 1, 9, 12, 14, 2, 0,
                6, 10, 13, 15, 3, 5, 8
            },
            {
                2, 1, 14, 7, 4, 10, 8, 13, 15,
                12, 9, 0, 3, 5, 6, 11
            }
        }
    };
    unsigned char row = 0;
    unsigned char col = 0;
    uint64_t sextet;// 6 bit
    uint32_t ret = 0;
    for (i = 0; i < 8; i++) {
        sextet = result & 277076930199552;
        sextet = sextet >> 42;
        row = row_from_sextet(sextet);
        col = col_from_sextet(sextet);
        tmp = s[i][row][col];
        if (i == 0)
            ret = ret | tmp;
        else
            ret = (ret << 4) | tmp;
        result = result << 6;
    }
    unsigned char p[] = {
        16, 7, 20, 21,
        29, 12, 28, 17,
        1, 15, 23, 26,
        5, 18, 31, 10,
        2, 8, 24, 14,
        32, 27, 3, 9,
        19, 13, 30, 6,
        22, 11, 4, 25
    };
    tmp = 0;
    uint32_t final = 0;
    for (i = 0; i < 32; i++) {
        tmp = ((ret >> (32 - p[i])) & 0b00000001);
        if (i == 0)
            final = tmp;
        else
            final = tmp | (final << 1);
    }
    return final;
}

void des_process_block(uint64_t in, unsigned char **subkeys, uint64_t *out, bool decode) {
    unsigned char ip[] = {
        58, 50, 42, 34, 26, 18, 10, 2,
        60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6,
        64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9, 1,
        59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5,
        63, 55, 47, 39, 31, 23, 15, 7
    };
    // block after initial permutation
    uint64_t in_p = 0;
    unsigned char tmp = 0;
    for (unsigned char i = 0; i < 64; i++) {
        tmp = ((in >> (64 - ip[i])) & 0b00000001);
        if (i == 0)
            in_p = tmp;
        else
            in_p = tmp | (in_p << 1);
    }

    unsigned int in_p_l = (unsigned int)(in_p >> 32);
    unsigned int in_p_r = (unsigned int)in_p;

    // encrypt / decrypt
    unsigned int in_p_l_0 = in_p_l;
    unsigned int in_p_r_0 = in_p_r;

    if (decode) {
        for (int i = 15; i >= 0; i--) {
            in_p_l = in_p_r_0;
            in_p_r = in_p_l_0 ^ des_fun(in_p_r_0, subkeys[i]);
            in_p_l_0 = in_p_l;
            in_p_r_0 = in_p_r;
        }
    }
    else {
        for (int i = 0; i < 16; i++) {
            in_p_l = in_p_r_0;
            in_p_r = in_p_l_0 ^ des_fun(in_p_r_0, subkeys[i]);
            in_p_l_0 = in_p_l;
            in_p_r_0 = in_p_r;
        }
    }
    in_p = in_p_r;
    in_p = (in_p << 32) | in_p_l;

    // final permutation
    unsigned char inv_ip[] = {
        40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25
    };

    tmp = 0;
    uint64_t final = 0;
    for (unsigned char i = 0; i < 64; i++) {
        tmp = ((in_p >> (64 - inv_ip[i])) & 0b00000001);
        if (i == 0)
            final = tmp;
        else
            final = tmp | (final << 1);
    }
    *out = final;
}

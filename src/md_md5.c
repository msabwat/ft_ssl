#include "ft_ssl.h"
#include "libft.h"

#include <stdio.h>

# define BUF_SIZE 64

#define ROTATE_LEFT(x, n) (((x) << (n)) | ((x) >> (32-(n))))

void md5_round (unsigned char *buf, char **result);

static void init_md5_registers(char **result) {
	// init registers
	uint32_t *val = (uint32_t *)(*result);
	*val = 0x67452301;
	val += 1;
	*val = 0xefcdab89;
	val += 1;
	*val = 0x98badcfe;
	val += 1;
	*val = 0x10325476;
	val += 1;
}

char *init_md5_hash(void) {
	char *result = ft_strnew(16);
	if (!result)
		return (NULL);

	init_md5_registers(&result);
	return (result);
}

static void print_md5_hash (char *result, size_t size) {
	uint32_t *val = (uint32_t *)result;
	unsigned char byte = 128;
	unsigned char buf[64];
	size_t len = 0;

	// finalize
	if ((size % 64 == 0) && (size != 0)) {
		buf[len] = byte;
		len++;
		while (len < 56) {
			buf[len] = 0;
			len++;
		}
		size_t bits_size = size * 8;
		ft_memcpy(buf + len, &bits_size, 8);
		md5_round(buf, &result);
	}
	uint8_t *p;
	// display result

    p=(uint8_t *)val;
	printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3]);
    p=(uint8_t *)(val + 1);
	printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3]);
    p=(uint8_t *)(val + 2);
	printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3]);
    p=(uint8_t *)(val + 3);
	printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3]);
}

void md5_round (unsigned char *buf, char **result) {
	uint32_t *res = (uint32_t *)(*result);
	uint32_t a = *res;
	uint32_t b = *(res + 1);
	uint32_t c = *(res + 2);
	uint32_t d = *(res + 3);

	uint32_t aa = a;
	uint32_t bb = b;
	uint32_t cc = c;
	uint32_t dd = d;

	uint32_t s[64] = {
		7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
		5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
		4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
		6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
	};

	uint32_t K[64] = {
		0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
		0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
		0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
		0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
		0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
		0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
		0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
		0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
		0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
		0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
		0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
		0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
		0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
		0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
		0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
		0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
	};

	uint32_t x[16];
	uint32_t *tmp = (uint32_t *)buf;
	for (int j = 0; j < 16; j++) {
		x[j] = *tmp;
		tmp++;
	}

	for (unsigned char i = 0; i < 64; i++) {		
		uint32_t F = 0;
		uint32_t g = 0;
		if (i <= 15) {
			F = (b & c) | ((~b) & d);
			g = i;
		}
		else if ((i >= 16) && (i <= 31)) {
			F = (d & b) | ((~d) & c);
			g = (5 * i + 1) % 16;
		}
		else if ((i >= 32) && (i <= 47)) {
			F = b ^ c ^ d;
			g = (3 * i + 5) % 16;
		}
		else if (i >= 48) {
			F = c ^ (b | (~d));
			g = (7 * i) % 16;
		}
		F = F + a + K[i] + x[g];
		a = d;
		d = c;
		c = b;
		b = b + ROTATE_LEFT(F, s[i]);
	}

	uint32_t *val = (uint32_t *)(*result);
	*val = aa + a;
	val += 1;
	*val = bb + b;
	val += 1;
	*val = cc + c;
	val += 1;
	*val = dd + d;
	val += 1;
}

void md5_block (char *chunk, uint64_t chunk_size, char **result, size_t total_size) {
	size_t len = chunk_size;
	unsigned char byte = 128;
	unsigned char buf[64];

	if (len <= 55) {
		ft_memcpy(buf, chunk, len);
		buf[len] = byte;
		len++;
		while (len < 56) {
			buf[len] = 0;
			len++;
		}
		size_t bits_size = total_size * 8;
		ft_memcpy(buf + len, &bits_size, 8);
		md5_round(buf, result);
	}
	else if (len == 64) {
		ft_memcpy(buf, chunk, len);
		md5_round(buf, result);
	}
	else if ((len > 55) && (len < 64)) {
		unsigned char long_buf[128];
		ft_memcpy(long_buf, chunk, len);
		long_buf[len] = byte;
		len++;
		while (len < 120) {
			long_buf[len] = 0;
			len++;
		}
		size_t bits_size = total_size * 8;
		ft_memcpy(long_buf + len, &bits_size, 8);
		md5_round(long_buf, result);
		md5_round(long_buf + 64, result);
	}
	else {
		printf("cannot call md5_block() on len:%zu\n", len);
		printf("should be <= 64\n");
		exit(1);
		//abort();
	}
}

void run_md5 (int ac, char **av, t_mode variant) {
	(void)variant;
	t_md_cbs cbs = { .md_hash=md5_block, .init_hash=init_md5_hash, .print_hash=print_md5_hash, .hash_name="MD5" };
	handle_md_options(ac, av, cbs);
	exit(0);
}

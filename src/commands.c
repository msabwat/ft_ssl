#include "ft_ssl.h"
#include "libft.h"
#include <stdio.h>
#include <limits.h>

static void print_usage(void) {
	printf("usage: ft_ssl command [flags] [file/string]\n");
}

static void check_cmds(const t_cmd cmds[], int ac, char **av, int *status) {
	int i = 0;
	for (;;) {
		if (cmds[i].name == NULL) {
			break ;
		}
		if (ft_strcmp(cmds[i].name, av[1]) == 0) {
			cmds[i].pf_runcmd(ac, av, cmds[i].variant);
			*status = 1;
			break ;
		}
		if (i == INT_MAX) {
			// should never happen
			*status = -1;
		}
		i++;
	}
}

static void print_available_commands(const t_cmd cmds[]) {
	int i = 0;
	for (;;) {
		if (cmds[i].name == NULL) {
			break ;
		}
		printf("%s\n", cmds[i].name);
		if (i == INT_MAX) {
			// should never happen
			break;
		}
		i++;
	}
}

static void print_mdhelp() {
	printf("Md Command options :\n");
	printf("-p, echo STDIN to STDOUT and append the checksum to STDOUT. \n");
	printf("-q, quiet mode. \n");
	printf("-r, reverse the format of the output. \n");
	printf("-s, print the sum of the given string. \n");
}

int validate_args(int ac, char **av) {
	if (ac == 1) {
		print_usage();
		return (0);
	}
	// valid commands
	const t_cmd std_cmds[] = {
		{ .name=NULL, .pf_runcmd=NULL, .variant=0 },
	};
	const t_cmd md_cmds[] = {
		{ .name="md5", .pf_runcmd=run_md5, .variant=0 },
		{ .name="sha256", .pf_runcmd=run_sha256, .variant=0 },
		{ .name=NULL, .pf_runcmd=NULL, .variant=0 },
	};
	const t_cmd cipher_cmds[] = {
		{ .name="base64", .pf_runcmd=run_base64, .variant=0 },
		{ .name="des", .pf_runcmd=run_des, .variant=DES_MODE },
		{ .name="des-cbc", .pf_runcmd=run_des, .variant=DES_CBC_MODE },
		{ .name="des-ecb", .pf_runcmd=run_des, .variant=DES_ECB_MODE },
		{ .name=NULL, .pf_runcmd=NULL, .variant=0 },
	};

	int status = 0;
	check_cmds(std_cmds, ac, av, &status);
	check_cmds(md_cmds, ac, av, &status);
	check_cmds(cipher_cmds, ac, av, &status);

	if (status == 0) {
		printf ("%s is not a valid command !\n", av[1]);
		printf("Available commands: \n\n");
		printf("Standard commands: \n");
		print_available_commands(std_cmds);
		printf("\n");
		printf("Message Digest commands: \n");
		print_available_commands(md_cmds);
		printf("\n");
		printf("Cipher commands: \n");
		print_available_commands(cipher_cmds);
		printf("\n");
		print_mdhelp();
	}
	else if (status == -1) {
		// should never happen
		exit(1);
	}

	return (1);
}

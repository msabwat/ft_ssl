#include "ft_ssl.h"
#include "libft.h"
#include <stdio.h>

void tmp_from_out(unsigned char *tmp, unsigned char *out, size_t offset, size_t size) {
    for (unsigned i = 0; i < size; i++) {
        tmp[i] = out[offset + i];
    }
}

static uint64_t octets_to_64(const unsigned char *chunk, size_t size_to_trsfrm, size_t size) {
    uint64_t res = 0;
    size_t i = size_to_trsfrm;
    for (i = size_to_trsfrm; i < size_to_trsfrm + 7; i++) {
        res = res | chunk[i];
        res = res << 8;
    }
    res = res | chunk[i];
    // will use CMS padding (https://asecuritysite.com/symmetric/padding_des)
    bool is_8_mult = (size % 8 == 0) ? 0: 1;
    size_t num = ((size / 8) + (1 * is_8_mult));
    if ((size % 8 != 0) && ((i + 1) / 8 == num)) {
        size_t rest = (num * 8) - size;
        i = 0;
        res = res >> (rest - 1) * 8;
        while ( i < rest - 1) {
            res = res | rest;
            res = res << 8;
            i++;
        }
        res = res | rest;
    }
    return res;
}

static int sanitize_chunk(int fd_in, const unsigned char *chunk, size_t size) {
    if (!chunk)
        return 0;
    unsigned char tmp[960];
    size_t i = 0;
    unsigned j = 0;
    bool eq_flag = 0;
    // 1. Remove non printable characters
    for (i = 0; i < size; i++) {
        if (ft_isprint(chunk[i])) {
            tmp[j++] = chunk[i];
        }
    }
    if (j < 960) {
        char c = 0;
        int rd = 0;
        while (42) {
            c = 0;
            rd = read(fd_in, &c, 1);
            if (rd == -1) {
                printf("error could not read file\n");
                return 0;
            }
            if (rd == 0)
                break;
            if (ft_isprint(c)) {
                tmp[j++] = c;
            }
            if (j % 4 == 0)
                break;            
        }
    }
    // 2.check if b64 input is correct
    for (i = 0; i < j; i++) {
        if ((chunk[i] == '=') && ((i == j - 1) || (i == j - 2)))
            eq_flag = 1;
        else if ((chunk[i] == '=') || (eq_flag == 1)) {
            printf("error: misplaced = \n");
            return 0;
        }
    }
    return 1;
}

static uint64_t *blocks_from_chunk(const unsigned char *chunk, size_t size) {
	/* 
		helper to simplify going from unsigned char [8] to uint64_t 
        and pad the last one if it's too smoll.
	 */
    bool is_8_mult = (size % 8 == 0) ? 0: 1;
    size_t num = ((size / 8) + (1 * is_8_mult));
	uint64_t *result = (uint64_t *)malloc(sizeof(uint64_t) * num);
	if (!result)
		return NULL;
    size_t size_arg = 0;
    for (unsigned i = 0; i < num; i++) {
        result[i] = octets_to_64(chunk, size_arg, size);
        size_arg += 8;
    }
	return result;
}

size_t code_des_blocks(const unsigned char *chunk, size_t size, unsigned char **out_buf, unsigned ind, t_cipher_opts opts, bool decode) {
    uint64_t *in_blocks = blocks_from_chunk(chunk, size);
    if (!in_blocks)
        return -1;
    uint64_t out = 0;
    uint64_t previous = 0;
    (void)previous;
    if (opts.cbc)
        previous = opts.iv;
    bool is_8_mult = (size % 8 == 0) ? 0: 1;
    size_t num = ((size / 8) + (1 * is_8_mult));
    for (unsigned i = 0; i < num; i++) {
        if (opts.cbc) {
            if (!decode) {
                des_process_block(in_blocks[i] ^ previous, opts.subkeys, &out, decode);
                previous = out;
            }
            else {
                des_process_block(in_blocks[i], opts.subkeys, &out, decode);
                out = previous ^ out;
                previous = out;
            }
        }
        else 
            des_process_block(in_blocks[i], opts.subkeys, &out, decode);
        (*out_buf)[ind] = (out >> 56) & 255;
        (*out_buf)[ind + 1] = (out >> 48) & 255;
        (*out_buf)[ind + 2] = (out >> 40) & 255;
        (*out_buf)[ind + 3] = (out >> 32) & 255;
        (*out_buf)[ind + 4] = (out >> 24) & 255;
        (*out_buf)[ind + 5] = (out >> 16) & 255;
        (*out_buf)[ind + 6] = (out >> 8) & 255;
        (*out_buf)[ind + 7] = out & 255;
        ind += 8;
    }
    free(in_blocks);
    return ind;
}

void encode_des(const unsigned char *chunk, size_t size, unsigned char **out_buf, t_cipher_opts opts) {
    unsigned ind = 0;
    unsigned i = 0;
    for (i = 0; i < size / 8; i++) {
        code_des_blocks(chunk + (i * 8), 8, out_buf, ind, opts, 0);
        ind += 8;
    }
    if (size % 8) {
        code_des_blocks(chunk + (i * 8), size % 8, out_buf, ind, opts, 0);
        ind += 8;
    }
    if (opts.b64) {
        /*
          input size is different from des output size,
          and des output will be used for b64 input
        */        
        unsigned char *tmp_buffer = (unsigned char *)ft_strnew(60);
        if (!tmp_buffer)
            return;
        unsigned char *out_buffer = (unsigned char *)ft_strnew(80);
        if (!out_buffer) {
            free(tmp_buffer);
            return;
        }
        i = 0;
        for (i = 0; i < ind / 60; i++) {
            tmp_from_out(tmp_buffer, *out_buf, i * 60, 60);
            encode_base64(tmp_buffer, 60, &out_buffer, opts);
            write(opts.out_fd, out_buffer, 80);
        }
        ft_bzero(out_buffer, 80);
        ft_bzero(tmp_buffer, 60);
        if (ind % 60) {
            size_t new_size = ((ind % 60) / 3) * 4 + 4;
            tmp_from_out(tmp_buffer, *out_buf, i * 60, ind % 60);
            encode_base64(tmp_buffer, ind % 60, &out_buffer, opts);
            write(opts.out_fd, out_buffer, new_size);
            // TODO: ajouter un '\n' comme openssl ?
        }
        free(out_buffer);
        free(tmp_buffer);
    }
}

size_t decode_des(const unsigned char *chunk, size_t chunk_size, unsigned char **out_buf, t_cipher_opts opts) {
    size_t coded_size = 0;
    if (opts.b64 == 1) {
        if (opts.cbc == 1) {} // TODO: use Salted instead of ours
        /*
          This won't check if the character is valid or not, 
          it will just remove non ascii characters.
        */
        if (!sanitize_chunk(opts.in_fd, chunk, chunk_size)) {
            printf("bad b64 input, cannot decode!\n");
            return -1;
        }
        unsigned char *out = (unsigned char *)ft_strnew(80);
        if (!out) {            
            return -1;
        }
        unsigned char *decoded_in = (unsigned char *)ft_strnew(960);
        if (!decoded_in) {
            ft_strdel((char **)&out);
            return -1;
        }
        unsigned char *tmp = (unsigned char *)chunk;
        size_t i = 0;
        size_t decoded_size = 0;
        unsigned inc = 60 / 4 * 3;
        for (i = 0; i < chunk_size / 60; i++) {
            decode_base64(tmp, 60, (unsigned char **)&out, opts);
            ft_memcpy(decoded_in + (i * inc), out, inc);
            // debug: write(1, out, 60 / 4 * 3);
            decoded_size += inc;
            tmp += 60;
        }
        ft_bzero(out, 80);
        if (chunk_size % 60) {
            decode_base64(tmp, chunk_size % 60, (unsigned char **)&out, opts);
            // FIXME: I don't know where that extra byte is coming from
            ft_memcpy(decoded_in + (i * inc), out, ((chunk_size % 60) / 4) * 3 - 1);
            // debug: write(1, out, ((chunk_size % 60) / 4) * 3 - 1);
            decoded_size += ((chunk_size % 60) / 4) * 3 - 1;
        }
        coded_size = code_des_blocks(decoded_in, decoded_size, out_buf, 0, opts, 1);
        ft_strdel((char **)&out);
        ft_strdel((char **)&decoded_in);
    }
    else {
        coded_size = code_des_blocks(chunk, chunk_size, out_buf, 0, opts, 1);
    }
    return coded_size;
}

void run_des(int ac, char **av, t_mode variant) {
	char *name = NULL;
	if (variant == DES_MODE)
		name = "des";
	else if (variant == DES_CBC_MODE)        
		name = "des-cbc";
	else if (variant == DES_ECB_MODE)
		name = "des-ecb";
	t_cipher_cbs cbs = { .out_buf=NULL, .encode=encode_des, .decode=decode_des, .cipher_name=name, .variant=variant };

    // new_buf=PPCM(60, 64)=960
    // new out_buf=PPCM(80, 64)=960
    // 960 * (80/60) = 1280
	cbs.out_buf = (unsigned char *)ft_strnew(960);
	if (!cbs.out_buf) {
		exit(1);
	}
	handle_cipher_options(ac, av, cbs);
	ft_strdel((char **)&(cbs.out_buf));
}

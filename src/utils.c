#include "ft_ssl.h"
#include "libft.h"
#include <stdint.h>
#include <stdio.h>

#ifdef DEBUG
void dbg_printBits(size_t size, void *ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;
    
    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
        printf(" ");
    }
    puts("");
}
#endif

static unsigned char asciichar_to_hex(char c) {
	if ((c >= '0') && (c <= '9'))
		return (c - 48);
	else if ((c >= 'A') && (c <= 'F'))
		return (c - 65 + 10);
	else if ((c >= 'a') && (c <= 'f'))
		return (c - 97 + 10);
	// not a hex string (should never happen)
	return -1;
}

uint64_t atohex(char *in) {
	// should sanitize the key before calling this function 
	size_t size = strlen(in);
	uint64_t res = 0;
	unsigned i = 0;
	for (i = 0; i < (unsigned)(size - 1); i++) {
		res = res | asciichar_to_hex(in[i]);
		res = res << 4;
	}
	res = res | asciichar_to_hex(in[i]);
	return res;
}

void memcpy_be(void *dst, const void *src, int nbytes) {
  uint8_t *d = dst;
  const uint8_t *s = src;
  
  d += nbytes;
  while (nbytes--) {
    *--d = *s++;
  }
}

int is_valid_b64(char c) {
	if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) || ((c >= '0') && (c <= '9')) || (c == '+') || (c == '/') || (c == '=')) {
		return (1);
	}
	else if (!ft_isprint(c))
		return (0);
	return (-1);
}

int is_valid_hex_opt(const char *in) {
	size_t len = ft_strlen(in);
	bool wrong = 0;
	for (size_t i = 0; i < len; i++) {
		if (!(((in[i] >= 'A') && (in[i] <= 'F'))
			  || ((in[i] >= '0') && (in[i] <= '9'))
              || ((in[i] >= 'a') && (in[i] <= 'f')))) {
			wrong = true;
			break ;
		}
	}
	// https://www.youtube.com/watch?v=V3y3QoFnqZc
	if (wrong == true)
		return (0);
	return (1);
}

uint64_t get_random_8bytes() {
	int fd = open("/dev/urandom", O_RDONLY);
	if (fd == -1) {
		printf("error: could not generate random salt\n");
		return(0);
	}
    unsigned char valid_bytes[16] = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
        0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    };
	unsigned char tmp[16];
	int rd = read(fd, tmp, 16);
	if (rd != 16) {
		printf("error: could not generate random salt\n");
		return(0);
	}
	uint64_t result = 0;
	int j = 15;
    while (j > 0) {
        result = result | valid_bytes[tmp[j] % 15];
		j--;
        result = result << 4;
	}
    result = result | valid_bytes[tmp[0] % 15];
	return (result);
}

static char *hash(unsigned char *input, size_t size) {
    char *hash = init_sha256_hash();
	if (!hash)
		return (0);
	size_t nb = size / 64;
	size_t remaining_size = size;
	for (size_t i = 0; i < nb; i++) {
		sha256_block((char *)input, 64, &hash, size);
		input += 64;
		remaining_size -= 64;
	}
    sha256_block((char *)input, remaining_size, &hash, size);
	// finalize
	unsigned char byte = 128;
	unsigned char buf[64];
	size_t len = 0;

	if ((remaining_size % 64 == 0) && (remaining_size != 0)) {
		buf[len] = byte;
		len++;
		while (len < 56) {
			buf[len] = 0;
			len++;
		}
		size_t bits_size = size * 8;
        memcpy_be(buf + len, &bits_size, 8);
		sha256_round(buf, &hash);
	}
    return hash;
}

static unsigned char *compute_block_sized_key(char *key) {
    size_t keysize = ft_strlen(key);
    char *new_key = ft_strnew(64);
    if (!new_key)
        return NULL;
    if (keysize > 64) {
        char *h = hash((unsigned char *)key, keysize);
        if (!h) {
            free(new_key);
            return NULL;
        }
        ft_strncpy(new_key, h, 32);
        free(h);
    }
    else if (keysize < 64) {
        ft_strncpy(new_key, key, keysize);
        // padding already done in ft_strnew
    }
    else {
        // key has the right size
        free(new_key);
        new_key = key;
    }
    return (unsigned char *)new_key;
}

static uint64_t key_from_hash(char *hash) {
    uint64_t result = 0;
    uint8_t *p;
    p = (uint8_t *)hash;
    for (unsigned i = 7; i > 0; i--) {
        result = result | p[i];
        result = result << 8;
    }
    result = result | p[0];

    return result;
}

/* 
   initial implementation. (hardcoded for sha256)
   TODO:
   - remove allocations
   - allow other hash sizes
   sources: from wikipedia pseudocode and RFC2104
 */
static uint64_t hmac_sha_2(char *key, uint64_t message, bool first) {
    uint64_t dKey = 0;
    unsigned char *new_message = NULL;
    unsigned char *new_key = NULL;
    unsigned char *i_key_pad = NULL;
    unsigned char *o_key_pad = NULL;
    unsigned char *tmp = NULL;
    unsigned char *to_hash = NULL;
    char *result = NULL;
    char *half = NULL;
    size_t size = 0;
    if (first) {
        size = 12;
        new_message = (unsigned char *)ft_strnew(size);
        if (!new_message)
            goto error;
        for (unsigned i = 0; i < 8; i++) {
            new_message[i] = message >> (56 - (8 * i));
        }
        new_message[8] = 0;
        new_message[9] = 0;
        new_message[10] = 0;
        new_message[11] = 1;
    }
    else {
        size = 8;
        new_message = (unsigned char *)ft_strnew(size);
        if (!new_message)
            goto error;
        for (unsigned i = 0; i < 8; i++) {
            new_message[i] = (message >> (56 - (8 * i)));
        }
//        write(1, new_message, 8);
//        write(1, "\n", 1);
    }

    // 1. computeBlockSizedKey (64 bytes)
    new_key = compute_block_sized_key(key);
    if (!new_key)
        goto error;

    // 2. o_key_pad ← block_sized_key xor [0x5c  blockSize]   // Outer padded key - 64 bytes
    o_key_pad = (unsigned char *)ft_strnew(64);
    if (!o_key_pad)
        goto error;
    for (unsigned i = 0; i < 64; i++) {
        o_key_pad[i] = 0x5c;
    }

    // 3. i_key_pad ← block_sized_key xor [0x36  blockSize]   // Inner padded key - 64 bytes
    i_key_pad = (unsigned char *)ft_strnew(64);
    if (!i_key_pad)
        goto error;
    for (unsigned i = 0; i < 64; i++) {
        i_key_pad[i] = 0x36;
    }
    
    for (unsigned i = 0; i < 64; i++) {
        i_key_pad[i] ^= new_key[i];
        o_key_pad[i] ^= new_key[i];
    }

    // 4. hash(o_key_pad ∥ hash(i_key_pad ∥ new_message))
    tmp = (unsigned char *)ft_strnew(64 + size);
    if (!tmp)
        goto error;
    ft_strncpy((char *)tmp, (char *)i_key_pad, 64);
    ft_strncpy((char *)(tmp + 64), (char *)new_message, size);

    half = hash(tmp, 64 + size);
    if (!half)
        goto error;

    to_hash = (unsigned char *)ft_strnew(64 + 32);
    if (!to_hash)
        goto error;
    ft_strncpy((char *)to_hash, (char *)o_key_pad, 64);
    ft_strncpy((char *)to_hash + 64, (char *)half, 32);

    result = hash(to_hash, 64 + 32);
    if (!result)
        goto error;

    // extract dKey from final hash
    dKey = key_from_hash(result); 

    // free
    if ((new_key != (unsigned char *)key) && new_key) {
        free(new_key);
    }

    if (o_key_pad)
        free(o_key_pad);
    if (i_key_pad)
        free(i_key_pad);
    if (new_message)
        free(new_message);
    if (to_hash)
        free(to_hash);
    if (tmp)
        free(tmp);
    if (half)
        free(half);
    if (result)
        free(result);
    return dKey;
error:
    printf("ERROR!:\n");
    if (o_key_pad)
        free(o_key_pad);
    if (i_key_pad)
        free(i_key_pad);
    if (new_message)
        free(new_message);
    if (to_hash)
        free(to_hash);
    if (tmp)
        free(tmp);
    if (half)
        free(half);
    if (result)
        free(result);
    return dKey;
}

uint64_t pbkdf2_dKlen_8bytes (char *password, uint64_t salt, unsigned count)
{
    if (count == 0)
        abort();
    // RFC 8018
    // 1. dkLen is constant here: 8 bytes
    /* 
       2. don't need these variables
       
       l = ceil(8 / 32) = 1
       r = 8
    */
    uint64_t dk = 0;
    /*
       3/4 . Only one block because l = 1; dk = T1
       and T1 = F (P, S, c, 1) = U_1 \xor U_2 \xor ... \xor U_c
       U_1 = PRF (P, S || INT (1)) ,
       U_2 = PRF (P, U_1) ,
       ...
       U_c = PRF (P, U_{c-1}) .
    */
    dk = hmac_sha_2(password, salt, 1);
    for (unsigned i = 0; i < count; i++) {
        dk = hmac_sha_2(password, dk, 0);
    }
    return dk;
}

/*
uint32_t *pbkdf_md5_hash(char *input, size_t size) {
	char *hash = init_md5_hash();
	if (!hash)
		return (NULL);
	char *tmp = input;
	size_t nb = size / 64;
	size_t remaining_size = size;
	for (size_t i = 0; i < nb; i++) {
		md5_block(tmp, 64, &hash, size);
		tmp += 64;
		remaining_size -= 64;
	}
	md5_block(tmp, remaining_size, &hash, size);
	// finalize
	unsigned char byte = 128;
	unsigned char buf[64];
	size_t len = 0;

	if ((remaining_size % 64 == 0) && (remaining_size != 0)) {
		buf[len] = byte;
		len++;
		while (len < 56) {
			buf[len] = 0;
			len++;
		}
		size_t bits_size = size * 8;
		ft_memcpy(buf + len, &bits_size, 8);
		md5_round(buf, &hash);
	}
	return ((uint32_t *)hash);
}

unsigned char *pbkdf1(uint32_t *(*hash_fun)(char *input, size_t size), char *password, char *salt, size_t iter) {
	unsigned char *drv_key = NULL;
	size_t len = ft_strlen(password);
	char *init = ft_strnew(8 + len);
	if (!init)
		return (NULL);
	ft_strncpy(init, salt, 8);
	ft_strncpy(init, password, len);
	
	uint32_t *previous = hash_fun(init, 8 + len);
	uint32_t *current = previous;
	uint32_t *tofree = previous;
	for (size_t i = 0; i < iter; i++) {
		tofree = previous;
		current = hash_fun((char *)previous, 8 + len);
		ft_strdel((char **)&tofree);
		previous = current;
	}
	drv_key = (unsigned char *)current;
	ft_strdel(&init);
	return (drv_key);
}
*/

#include "ft_ssl.h"
#include "libft.h"
#include <stdio.h>

static void handle_fd(unsigned char print, unsigned char quiet, unsigned char reverse, t_md_cbs cbs, int fd, char *filename) {
	int rd = 0;
	char buf[64];
	size_t total = 0;
	char *result = cbs.init_hash();
	
	if ((print) && (fd == 0)) {
		printf("%s(\"", cbs.hash_name);
		while (42) {
			rd = read(fd, buf, 64);
			if (rd == -1) {
				printf("error could not read file\n");
				exit(1);
			}
			if ((rd == 0) && !total) {
				cbs.md_hash(buf, rd, &result, total);
				break ;
			}
			else if (rd == 0)
				break ;
			total += rd;
			cbs.md_hash(buf, rd, &result, total);
			char p_buf[65];
			ft_memcpy(p_buf, buf, rd);
			p_buf[rd] = '\0';
			printf("%s", p_buf);
		}
		printf("\") = ");
		cbs.print_hash(result, total);
		printf("\n");
	}
	else if (quiet) {
		while (42) {
			rd = read(fd, buf, 64);
			if (rd == -1) {
				printf("error could not read file\n");
				exit(1);
			}
			if ((rd == 0) && !total) {
				cbs.md_hash(buf, rd, &result, total);
				break ;
			}
			else if (rd == 0)
				break ;
			total += rd;
			cbs.md_hash(buf, rd, &result, total);
		}
		cbs.print_hash(result, total);
		printf("\n");
	}
	else if (reverse) {
		while (42) {
			rd = read(fd, buf, 64);
			if (rd == -1) {
				printf("error could not read file\n");
				exit(1);
			}
			if ((rd == 0) && !total) {
				cbs.md_hash(buf, rd, &result, total);
				break ;
			}
			else if (rd == 0)
				break ;
			total += rd;
			cbs.md_hash(buf, rd, &result, total);
		}
		cbs.print_hash(result, total);
		if (fd == 0)
			printf(" stdin\n");
		else
			printf(" %s\n", filename);
	}
	else {
		while (42) {
			rd = read(fd, buf, 64);
			if (rd == -1) {
				printf("error could not read file\n");
				exit(1);
			}
			if ((rd == 0) && !total) {
				cbs.md_hash(buf, rd, &result, total);
				break ;
			}
			else if (rd == 0)
				break ;
			total += rd;
			cbs.md_hash(buf, rd, &result, total);
		}
		if (fd == 0) {
			printf("%s(stdin) = ", cbs.hash_name);
			cbs.print_hash(result, total);
			printf("\n");
		}
		else {
			printf("%s(%s) = ", cbs.hash_name, filename);
			cbs.print_hash(result, total);
			printf("\n");
		}
	}
	ft_strdel(&result);
}

static void handle_str(unsigned char quiet, unsigned char reverse, t_md_cbs cbs, char *input) {
	size_t len = ft_strlen(input);
	size_t nb = len / 64;
	char *result = cbs.init_hash();
	char *tmp = input;
	
	for (size_t i = 0; i < nb; i++) {
		cbs.md_hash(tmp, 64, &result, len);
		tmp += 64;
	}
	cbs.md_hash(tmp, ft_strlen(tmp), &result, len);
	if (quiet) {
		cbs.print_hash(result, len);
		printf("\n");
	}
	else if (reverse) {
		cbs.print_hash(result, len);
		printf(" \"%s\"\n", input);
	}
	else {
		printf("%s(\"%s\") = ", cbs.hash_name, input);
		cbs.print_hash(result, len);
		printf("\n");
	}
	ft_strdel(&result);
}

void handle_md_options(int ac, char **av, t_md_cbs cbs) {
	if (ac == 2) {
		handle_fd(0, 0, 0, cbs, 0, NULL);
	}
	else {
		unsigned char print = 0;
		unsigned char quiet = 0;
		unsigned char reverse = 0;

		// options pass
		for (int i = 2; i < ac; i++) {
			if (ft_strcmp(av[i], "-p") == 0) {
				print = 1;
			}
			else if (ft_strcmp(av[i], "-q") == 0) {
				quiet = 1;
			}
			else if (ft_strcmp(av[i], "-r") == 0) {
				reverse = 1;
			}
		}

		// arguments pass
		if (print) {
			handle_fd(print, 0, 0, cbs, 0, NULL);
		}
		for (int i = 2; i < ac; i++) {
			if (ft_strcmp(av[i], "-s") == 0) {
				if (av[i + 1] != NULL) { 
					handle_str(quiet, reverse, cbs, av[i + 1]);
					i++;
				}
				else {
					// try to open "-s"
					int fd = open("-s", O_RDONLY);
					if (fd == -1) {
						printf("error: could not open -s\n");
						exit(1);
					}
					handle_fd(0, quiet, reverse, cbs, fd, "-s");
				}
			}
			else if ((ft_strcmp(av[i], "-q") == 0) && (quiet < 2)) {
				quiet += 1;
			}
			else if ((ft_strcmp(av[i], "-p") == 0) && (print < 2)) {
				print += 1;
			}
			else if ((ft_strcmp(av[i], "-r") == 0) && (reverse < 2)) {
				reverse += 1;
			}
			else {
				int fd = open(av[i], O_RDONLY);
				if (fd == -1) {
					printf("error: could not open %s\n", av[i]);
					exit(1);
				}
				handle_fd(0, quiet, reverse, cbs, fd, av[i]);
			}
		}
	}
}

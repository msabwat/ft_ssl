#include "ft_ssl.h"
#include "libft.h"
#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>

static void handle_des_input(int fd_in, int fd_out, t_cipher_opts opts, t_cipher_cbs cbs) {
	int rd = 0;
    // new_buf=PPCM(60, 64)=960
    // new out_buf=PPCM(80, 64)=960
    // 960 * (80/60) = 1280
    size_t size = 0;
    unsigned char *buf = NULL;
    buf = (unsigned char *)ft_strnew(960);
    if (!buf) {
        return;
    }
    // to hold getpass return
    char *tmp = NULL;
    char *password = NULL;
    char *p1 = NULL;
    if (cbs.variant ==  DES_CBC_MODE) {
        opts.cbc = true;
        if (opts.iv == 0) {
			printf("error: cbc needs an initialization vector\n");
            ft_strdel((char **)&(cbs.out_buf));
            if (opts.in_password)
                ft_strdel(&opts.in_password);
			exit(1);            
        }
    }
	if ((opts.key == 0) && (opts.in_password == NULL)) {
        printf("warning: getpass is obsolete! (will remove after evaluation)\n");
		tmp = getpass("No key or password provided, please enter password:\n");
        if (!tmp) {
			printf("error, getpass returned an error\n");
            ft_strdel((char **)&(cbs.out_buf));
			exit(1);
		}
        size_t p1len = ft_strlen(tmp);
        p1 = ft_strnew(p1len);
        if (!p1) {
			printf("error, no memory\n");
            ft_strdel((char **)&(cbs.out_buf));
			exit(1);
        }
        ft_strncpy(p1, tmp, p1len);
        tmp = getpass("can you please confirm the password?\n");
        if (!tmp) {
			printf("error, getpass returned an error\n");
            ft_strdel((char **)&(cbs.out_buf));
            free(p1);
			exit(1);
		}
        size_t pwdlen = ft_strlen(tmp);
        password = ft_strnew(pwdlen);
        if (!password) {
			printf("error, no memory\n");
            ft_strdel((char **)&(cbs.out_buf));
            free(p1);
			exit(1);
        }
        ft_strncpy(password, tmp, pwdlen);
        if ((pwdlen == p1len) && (pwdlen != 0)) {
            if (ft_strncmp(p1, password, p1len) == 0) {
                (void)0;
                // printf("all set! #%s#%s#\n", p1, password);
            }
            else
                goto err;
        }
        else {
        err:
			printf("oops, the passwords did not match... aborting\n");
            ft_strdel((char **)&(cbs.out_buf));
            free(p1);
            free(password);
			exit(1);
        }    
	}
	else if (opts.in_password) {
        size_t size = ft_strlen(opts.in_password);
        password = ft_strnew(size);        
        if (!password)
            goto cleanup;
        ft_strncpy(password, opts.in_password, size);
    }
	if (opts.key == 0) {
		if (opts.salt) {
            opts.key = pbkdf2_dKlen_8bytes(password, opts.salt, 1);
            printf("KEY=%" PRIx64 "\n", opts.key);
        }
		else {
            opts.salt = get_random_8bytes();
            printf("SALT=%" PRIx64 "\n", opts.salt);
            opts.key = pbkdf2_dKlen_8bytes(password, opts.salt, 1);
            printf("KEY=%" PRIx64 "\n", opts.key);
		}
	}
    opts.subkeys = malloc(sizeof(unsigned char *) * 16);
    if(!opts.subkeys)
        goto cleanup;
    for (unsigned i = 0; i < 16; i++) {
        opts.subkeys[i] = malloc(sizeof(unsigned char) * 6);
        if (!opts.subkeys[i]) {
            unsigned j = 0;
            while (j < i)
                free(opts.subkeys[j++]);
            goto cleanup;
        }
    } 
    create_subkeys(opts.key, opts.subkeys);
	while (42) {
        ft_bzero(cbs.out_buf, 960);
        ft_bzero(buf, 960);
        rd = read(fd_in, buf, 960);
		if (rd == -1) {
			printf("error could not read file\n");
            goto cleanup;
		}
		if (rd == 0)
			break;
        
        if (opts.encode) {
            cbs.encode(buf, rd, &cbs.out_buf, opts);
            if (!opts.b64) {
                if (rd % 8)
                    write(fd_out, cbs.out_buf, 8 * (rd/8 + 1));
                else
                    write(fd_out, cbs.out_buf, rd);
            }// else handled in src/des.c
        }
        else {
            size = cbs.decode(buf, rd, &cbs.out_buf, opts);
            write(fd_out, cbs.out_buf, size);
        }
	}
    for (int i = 0; i < 16; i++) {
        if (opts.subkeys[i])
            free(opts.subkeys[i]);
    }
    if (opts.subkeys)
        free(opts.subkeys);
    if (password)
        free(password);
    free(p1);
    free(buf);
    return ;
cleanup:
    for (int i = 0; i < 16; i++) {
        if (opts.subkeys[i])
            free(opts.subkeys[i]);
    }
    if (opts.subkeys)
        free(opts.subkeys);
    if (password)
        free(password);
    free(p1);
    free(buf);
    exit(1);
}

static void handle_b64_input(int fd_in, int fd_out, t_cipher_opts opts, t_cipher_cbs cbs) {
	int rd = 0;
	unsigned char buf[60];
	int j = 0;
	int i = 0;
	bool eq_flag = 0;

	while (42) {
		j = 0;
		i = 0;
		ft_bzero(buf, 60);
		ft_bzero(cbs.out_buf, 80);
		rd = read(fd_in, buf, 60);

		if (rd == -1) {
			printf("error could not read file\n");
			exit(1);
		}
		if (rd == 0)
			break;
		if (opts.encode) {
			cbs.encode(buf, rd, &cbs.out_buf, opts);
			write(fd_out, cbs.out_buf, ft_strlen((char *)cbs.out_buf));
		}
		else {
			unsigned char tmp[60];
			for (i = 0; i < rd; i++) {
				if (ft_isprint(buf[i])) {
					tmp[j++] = buf[i];
				}
			}
			if (j < 60) {
				char c = 0;
				int rd = 0;
				while (42) {
					c = 0;
					rd = read(fd_in, &c, 1);
					if (rd == -1) {
						printf("error could not read input\n");
						exit(1);
					}
					if (rd == 0)
						break;
					if (ft_isprint(c)) {
						tmp[j++] = c;
					}
					if (j % 4 == 0)
						break;
				}
			}
			// check if it is valid
			for (i = 0; i < j; i++) {
				if ((buf[i] == '=') && ((i == j - 1) || (i == j - 2)))
					eq_flag = 1;
				else if ((buf[i] == '=') || (eq_flag == 1)) {
					printf("error: misplaced = \n");
					ft_strdel((char **)&(cbs.out_buf));
					exit(1);
				}
			}
			cbs.decode(tmp, j, &cbs.out_buf, opts);
			write(fd_out, cbs.out_buf, (j/4) * 3);
		}
	}
	if (opts.encode) {
		ft_putstr_fd("\n", fd_out);	
	}
}

static void handle_fd(int fd_in, int fd_out, t_cipher_opts opts, t_cipher_cbs cbs) {
	if (ft_strcmp(cbs.cipher_name, "base64") == 0) {
		handle_b64_input(fd_in, fd_out, opts, cbs);
	}
	else {
		handle_des_input(fd_in, fd_out, opts, cbs);
	}
}

void handle_cipher_options(int ac, char **av, t_cipher_cbs cbs) {
	t_cipher_opts opts = {
		.encode = true,
		.b64 = false,
        .cbc = false,
        .key = 0,
        .iv = 0,
        .salt = 0,
		.in_password = NULL,
        .in_fd = 0,
        .out_fd = 1,
        .subkeys = NULL,
	};
    bool alloc_pwd = false;
	if (ac == 2) {
		handle_fd(0, 1, opts, cbs);
	}
	else {
		opts.encode = true;

		for (int i = 2; i < ac; i++) {
			if (ft_strcmp(av[i], "-d") == 0) {
				opts.encode = false;
			}
			else if ((ft_strcmp(av[i], "-a") == 0) && (cbs.variant > 0)) {
				opts.b64 = true;
			}
			else if ((ft_strcmp(av[i], "-k") == 0) && (cbs.variant > 0)) {
				if (av[i + 1]) {
                    if (!is_valid_hex_opt(av[i + 1])) {
                        ft_strdel((char **)&(cbs.out_buf));
                        if (alloc_pwd)
                            ft_strdel(&opts.in_password);
                        printf("key is invalid, should be hexadecimal!\n");
                        exit(1);
                    }
					size_t len = ft_strlen(av[i + 1]);
					uint64_t key = 0;
					if (len < 16) {
                        printf("key is too short, padding with zero bytes to length\n");
                        key = atohex(av[i + 1]);
						key = key << ((16 - len) * 4);
					}
                    else if (len == 16) {
                        key = atohex(av[i + 1]);
                    }
                    else {
                        printf("key too long, ignoring excess\n");
                        char *buf = ft_strnew(16);
                        ft_strncpy(buf, av[i + 1], 16);
                        key = atohex(buf);
                        ft_strdel(&buf);
                    }
					opts.key = key;
					i += 1;
				}
				else {
					printf("error missing key\n");
					ft_strdel((char **)&(cbs.out_buf));
                    if (alloc_pwd)
                        ft_strdel(&opts.in_password);
					exit(1);
				}
			}
			else if ((ft_strcmp(av[i], "-p") == 0) && (cbs.variant > 0)) {
				if (av[i + 1]) {                    
					size_t len = ft_strlen(av[i +1]);
					opts.in_password = ft_strnew(len);
					if (!opts.in_password){
						printf("error no memory\n");
						// del
						ft_strdel((char **)&(cbs.out_buf));
						exit(1);
					}
                    alloc_pwd = true;
					ft_memcpy(opts.in_password, av[i + 1], len);
					i += 1;
				}
				else {                    
					printf("error missing password\n");
					// del
					ft_strdel((char **)&(cbs.out_buf));
					exit(1);
				}
                continue;
			}
			else if ((ft_strcmp(av[i], "-v") == 0) && (cbs.variant > 0)) {
                // no iv needed except for des-cbc mode
				if (av[i + 1]) {
                    if (!is_valid_hex_opt(av[i + 1])) {
                        ft_strdel((char **)&(cbs.out_buf));
                        if (alloc_pwd)
                            ft_strdel(&opts.in_password);
                        printf("initialization vector is invalid, should be hexadecimal!\n");
                        exit(1);
                    }
                    size_t len = ft_strlen(av[i + 1]);
					uint64_t iv = 0;
					if (len < 16) {
                        printf("iv is too short, padding with zero bytes to length\n");
                        iv = atohex(av[i + 1]);
						iv = iv << ((16 - len) * 4);
					}
                    else if (len == 16) {
                        iv = atohex(av[i + 1]);
                    }
                    else {
                        printf("iv too long, ignoring excess\n");
                        char *buf = ft_strnew(16);
                        ft_strncpy(buf, av[i + 1], 16);
                        iv = atohex(buf);
                        ft_strdel(&buf);
                    }
					opts.iv = iv;
					i += 1;
				}
				else {
					printf("error missing initialization vector\n");
					// del
					ft_strdel((char **)&(cbs.out_buf));
                    if (alloc_pwd)
                        ft_strdel(&opts.in_password);
					exit(1);
				}
                continue;
			}
			else if ((ft_strcmp(av[i], "-s") == 0) && (cbs.variant > 0)) {
				if (av[i + 1]) {
                    if (!is_valid_hex_opt(av[i + 1])) {
                        ft_strdel((char **)&(cbs.out_buf));
                        if (alloc_pwd)
                            ft_strdel(&opts.in_password);
                        printf("salt is invalid, should be hexadecimal!\n");
                        exit(1);
                    }
                    size_t len = ft_strlen(av[i + 1]);
					uint64_t salt = 0;
					if (len < 16) {
                        printf("salt is too short, padding with zero bytes to length\n");
                        salt = atohex(av[i + 1]);
						salt = salt << ((16 - len) * 4);
					}
                    else if (len == 16) {
                        salt = atohex(av[i + 1]);
                    }
                    else {
                        printf("salt too long, ignoring excess\n");
                        char *buf = ft_strnew(16);
                        ft_strncpy(buf, av[i + 1], 16);
                        salt = atohex(buf);
                        ft_strdel(&buf);
                    }
					opts.salt = salt;
					i += 1;
				}
				else {
					printf("error missing salt\n");
					// del
					ft_strdel((char **)&(cbs.out_buf));
                    if (alloc_pwd)
                        ft_strdel(&opts.in_password);
					exit(1);
				}
                continue;
			}
			else if (ft_strcmp(av[i], "-e") == 0) {
				// option is valid, but it doesn't do anything (default)
				continue;
			}
			else if (ft_strcmp(av[i], "-i") == 0) {
				if (av[i+1]) {
					opts.in_fd = open(av[i+1], O_RDONLY);
					if (opts.in_fd == -1) {
						printf("error could not open input file: %s\n", av[i+1]);
						ft_strdel((char **)&(cbs.out_buf));
                        if (alloc_pwd)
                            ft_strdel(&opts.in_password);
						exit(1);
					}
					i++;
				}
				else {
					printf("error missing input file\n");
					ft_strdel((char **)&(cbs.out_buf));
                    if (alloc_pwd)
                        ft_strdel(&opts.in_password);
					exit(1);
				}
			}
			else if (ft_strcmp(av[i], "-o") == 0) {
				if (av[i+1]) {
					opts.out_fd = open(av[i+1], O_CREAT | O_WRONLY, 0640);
					if (opts.out_fd == -1) {
						printf("error could not open output file: %s\n", av[i+1]);
						ft_strdel((char **)&(cbs.out_buf));
                        if (alloc_pwd)
                            ft_strdel(&opts.in_password);
						exit(1);
					}
					i++;
				}
				else {
					printf("error missing output file\n");
					ft_strdel((char **)&(cbs.out_buf));
                    if (alloc_pwd)
                        ft_strdel(&opts.in_password);                    
					exit(1);
				}
			}
			else {
				printf("error %s is not a valid option for %s\n", av[i], cbs.cipher_name);
				ft_strdel((char **)&(cbs.out_buf));
                if (alloc_pwd)
                    ft_strdel(&opts.in_password);
				exit(1);
			}
		}
		if (!opts.encode) {
			handle_fd(opts.in_fd, opts.out_fd, opts, cbs);
		}
		else {
			handle_fd(opts.in_fd, opts.out_fd, opts, cbs);
		}
	}
    if (alloc_pwd) {
        ft_strdel(&opts.in_password);
    }
}

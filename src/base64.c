#include "ft_ssl.h"
#include "libft.h"

#include <stdio.h>

void encode_base64(const unsigned char *input, size_t len, unsigned char **out_buf,  t_cipher_opts opts) {
	(void)opts;
	unsigned char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	int rest = len % 3;
	unsigned int tmp = 0;
	int ind = 0;
	size_t i = 0;
	unsigned char sextets[4]; // set of 6-bit octets

	for (i = 0; i < (len - rest); i += 3) {
		tmp = (input[i] << 16) | (input[i + 1] << 8) | input[i + 2];
		sextets[0] = (tmp >> 18) & 63;
		sextets[1] = (tmp >> 12) & 63;
		sextets[2] = (tmp >> 6) & 63;
		sextets[3] = tmp & 63;

		(*out_buf)[ind] = b64[sextets[0]];
		(*out_buf)[ind + 1] = b64[sextets[1]];
		(*out_buf)[ind + 2] = b64[sextets[2]];
		(*out_buf)[ind + 3] = b64[sextets[3]];
		ind += 4;
	}
	if (rest == 2) {
		tmp = (input[i] << 8) | input[i + 1];
		sextets[0] = (tmp >> 10) & 63;
		sextets[1] = (tmp >> 4) & 63;
		sextets[2] = (tmp << 2) & 63;
		sextets[3] = 0;

		(*out_buf)[ind] = b64[sextets[0]];
		(*out_buf)[ind + 1] = b64[sextets[1]];
		(*out_buf)[ind + 2] = b64[sextets[2]];
		(*out_buf)[ind + 3] = '=';
	}
	else if (rest == 1) {
		tmp = input[i];
		sextets[0] = (tmp >> 2) & 63;
		sextets[1] = (tmp << 4) & 63;
		sextets[2] = 0;
		sextets[3] = 0;

		(*out_buf)[ind] = b64[sextets[0]];
		(*out_buf)[ind + 1] = b64[sextets[1]];
		(*out_buf)[ind + 2] = '=';
		(*out_buf)[ind + 3] = '=';
	}
} 

size_t decode_base64(const unsigned char *input, size_t len, unsigned char **out_buf,  t_cipher_opts opts) {
	(void)opts;
	if (len % 4 != 0) {
		printf("Could not decode, b64 input does not have the right size %d\n", (int)len);
        ft_strdel((char **)out_buf);
		exit(1);
	}

	unsigned char inv_b64[] = {
	    62, -1, -1, -1, 63,
	    52, 53, 54, 55, 56, 57, 58,59, 60, 61,
		-1, -1, -1, -1, -1, -1, -1,
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		21, 22, 23, 24, 25,
		-1, -1, -1, -1, -1, -1,
		26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46,
		47, 48, 49, 50, 51
	};

	unsigned char sextets[4];
	unsigned int tmp = 0;
	int ind = 0;
	int ret = 0;
	unsigned int i = 0;

	unsigned char result[3];
	unsigned char padding = 0;
	while (42) {
		if (i == len)
			break;
		tmp = 0;
		for (int j = 0; j < 4; j++) {
			ret = is_valid_b64(input[i]);
			if (ret == 1) {
				if (input[i] == '=') {
					sextets[j] = 0;
					if (j == 2) {
						padding += 1;
					}
					if (j == 3) {
						padding += 1;
					}
				}
				else
					sextets[j] = (inv_b64[input[i] - 43] << 2);
				i += 1;
			}
			else if (ret == 0)
				i += 1;
			else {
				ft_strdel((char **)out_buf);
				printf("Could not decode, %c is not a valid b64 input\n", input[i]);
				exit(1);
			}
		}
		tmp = (sextets[0] << 20) | (sextets[1] << 14) | (sextets[2] << 8) | (sextets[3] << 2);
		tmp = tmp >> 4;

		result[0] = ((tmp >> 16) & 255);
		result[1] = ((tmp >> 8) & 255);
		result[2] = (tmp & 255);
		if (padding == 2)
			(*out_buf)[ind] = result[0];
		else if (padding == 1) {
			(*out_buf)[ind] = result[0];
			(*out_buf)[ind + 1] = result[1];
		}
		else {
			(*out_buf)[ind] = result[0];
			(*out_buf)[ind + 1] = result[1];
			(*out_buf)[ind + 2] = result[2];
		}
		ind += 3;
	}
    // hack in decode_des
    return 0;
}

void run_base64(int ac, char **av, t_mode variant) {
	(void)variant;
	t_cipher_cbs cbs = { .out_buf=NULL, .encode=encode_base64, .decode=decode_base64, .cipher_name="base64" };
	cbs.out_buf = (unsigned char *)ft_strnew(80);
	if (!cbs.out_buf) {
		exit(1);
	}
	handle_cipher_options(ac, av, cbs);
	ft_strdel((char **)&(cbs.out_buf));
}

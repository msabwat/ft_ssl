#include "ft_ssl.h"
#include "libft.h"

#include <stdio.h>

# define BUF_SIZE 64

#define ROTATE_RIGHT(x, n) (((x) >> (n)) | ((x) << (32-(n))))

static void init_sha256_registers(char **result) {
	(void)result;
	// init registers
	
	uint32_t *val = (uint32_t *)(*result);
	*val = 0x6a09e667;
	val += 1;
	*val = 0xbb67ae85;
	val += 1;
	*val = 0x3c6ef372;
	val += 1;
	*val = 0xa54ff53a;
	val += 1;
	*val = 0x510e527f;
	val += 1;
	*val = 0x9b05688c;
	val += 1;
	*val = 0x1f83d9ab;
	val += 1;
	*val = 0x5be0cd19;
	val += 1;
}

char *init_sha256_hash(void) {
	char *result = ft_strnew(32);
	if (!result)
		return (NULL);

	init_sha256_registers(&result);
	return (result);
}

static void print_sha256_hash (char *result, size_t size) {
	unsigned char byte = 128;
	unsigned char buf[64];
	size_t len = 0;
	uint32_t *val = (uint32_t *)result;

	// finalize
	if ((size % 64 == 0) && (size != 0)) {
		buf[len] = byte;
		len++;
		while (len < 56) {
			buf[len] = 0;
			len++;
		}
		size_t bits_size = size * 8;
		memcpy_be(buf + len, &bits_size, 8);
		sha256_round(buf, &result);
	}
	uint8_t *p;

	// display result
    p=(uint8_t *)val;
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 1);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 2);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 3);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 4);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 5);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 6);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
	p=(uint8_t *)(val + 7);
	printf("%2.2x%2.2x%2.2x%2.2x", p[3], p[2], p[1], p[0]);
}

void sha256_round (unsigned char *buf, char **result) {
	uint32_t *res = (uint32_t *)(*result);
	uint32_t a = *res;
	uint32_t b = *(res + 1);
	uint32_t c = *(res + 2);
	uint32_t d = *(res + 3);
	uint32_t e = *(res + 4);
	uint32_t f = *(res + 5);
	uint32_t g = *(res + 6);
	uint32_t h = *(res + 7);

	uint32_t aa = a;
	uint32_t bb = b;
	uint32_t cc = c;
	uint32_t dd = d;
	uint32_t ee = e;
	uint32_t ff = f;
	uint32_t gg = g;
	uint32_t hh = h;

	
	uint32_t K[64] = {
		0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
	};

	uint32_t w[64];

	ft_bzero(w, 64 * 4);
	for (int i = 0; i < 16; i++) {
		memcpy_be(&(w[i]), buf + (i * 4), 4);
	}
	for (int j = 16; j < 64; j++) {
		uint32_t s0 = ROTATE_RIGHT(w[j - 15], 7) ^ ROTATE_RIGHT(w[j - 15], 18) ^ ((w[j - 15]) >> 3);
		uint32_t s1 = ROTATE_RIGHT(w[j - 2], 17) ^ ROTATE_RIGHT(w[j -2], 19) ^ ((w[j - 2]) >> 10);
		w[j] = w[j - 16] + s0 + w[j - 7] + s1;
	}
	for (int i = 0; i < 64; i++) {
		uint32_t S1 = ROTATE_RIGHT(e, 6) ^ ROTATE_RIGHT(e, 11) ^ ROTATE_RIGHT(e, 25);
		uint32_t ch = ((e & f) ^ ((~e) & g));
		uint32_t temp1 = h + S1 + ch + K[i] + w[i];
		uint32_t S0 = ROTATE_RIGHT(a, 2) ^ ROTATE_RIGHT(a, 13) ^ ROTATE_RIGHT(a, 22);
		uint32_t maj = (a & b) ^ (a & c) ^ (b & c);
		uint32_t temp2 = S0 + maj;

		h = g;
		g = f;
		f = e;
		e = d + temp1;
		d = c;
		c = b;
		b = a;
		a = temp1 + temp2;
	}

	uint32_t *val = (uint32_t *)(*result);
	*val = aa + a;
	val += 1;
	*val = bb + b;
	val += 1;
	*val = cc + c;
	val += 1;
	*val = dd + d;
	val += 1;
	*val = ee + e;
	val += 1;
	*val = ff + f;
	val += 1;
	*val = gg + g;
	val += 1;
	*val = hh + h;
	val += 1;
}

void sha256_block (char *chunk, uint64_t chunk_size, char **result, size_t total_size) {
	size_t len = chunk_size;
	unsigned char byte = 128;
	unsigned char buf[64];

	if (len <= 55) {
		ft_memcpy(buf, chunk, len);
		buf[len] = byte;
		len++;
		while (len < 56) {
			buf[len] = 0;
			len++;
		}
		size_t bits_size = total_size * 8;
		memcpy_be(buf + len, &bits_size, 8);
		sha256_round(buf, result);
	}
	else if (len == 64) {
		ft_memcpy(buf, chunk, 64);
		sha256_round(buf, result);
	}
	else if ((len > 55) && (len < 64)) {
		unsigned char long_buf[128];
		ft_memcpy(long_buf, chunk, len);
		long_buf[len] = byte;
		len++;
		while (len < 120) {
			long_buf[len] = 0;
			len++;
		}
		size_t bits_size = total_size * 8;
		memcpy_be(long_buf + len, &bits_size, 8);
		sha256_round(long_buf, result);
		sha256_round(long_buf + 64, result);
	}
	else {
		printf("cannot call sha256_block() on len:%zu\n", len);
		printf("should be <= 64\n");
		exit(1);
		//abort();
	}
}

void run_sha256 (int ac, char **av, t_mode variant) {
	(void)variant;
	t_md_cbs cbs = { .md_hash=sha256_block, .init_hash=init_sha256_hash, .print_hash=print_sha256_hash, .hash_name="SHA256" };
	handle_md_options(ac, av, cbs);
	exit(0);
}

NAME=ft_ssl

FLAGS=-Wall -Wextra -Werror -g -fsanitize=address,undefined -DDEBUG

CC=clang

INC=-I.

LIBFT_PATH=libft/

LIBFT_INC=-I$(LIBFT_PATH)

LIBFT_LINK_FLAGS=-L $(LIBFT_PATH) -lft 

SRC_NAME=	commands.c \
			utils.c \
			md_md5.c \
			md_sha256.c \
			md_options.c \
			base64.c \
			des.c \
			des_utils.c \
			cipher_options.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

TEST=

.PHONY: all clean fclean re

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g -fsanitize=address,undefined
test: TEST = test
test: makedir $(NAME)


$(NAME): $(OBJ_PATH)/main.o $(OBJS)
	$(MAKE) -C $(LIBFT_PATH)
	$(CC) $(FLAGS) $(OBJ_PATH)/main.o $(LIBFT_INC) $(OBJS) $(INC) $(LIBFT_LINK_FLAGS) -o $(NAME)

$(OBJ_PATH)/main.o: main.c
	 $(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c main.c -o $(OBJ_PATH)/main.o 
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(OBJ_PATH)
	rm -fr main.o
	$(MAKE) clean -C $(LIBFT_PATH)

fclean: clean
	rm -fr $(NAME)
	$(MAKE) fclean -C $(LIBFT_PATH)

re: fclean all
